const entriesJSON = '<%= options.blogEntries %>'
const entries = JSON.parse(entriesJSON.replace(/\\\\n/g,'\\n'))

export default (ctx, inject) => {
  ctx.$blogEntries = entries
  inject('blogEntries', entries)
}