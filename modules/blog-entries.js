import path from 'path'
import fs from 'fs'

export default function(moduleOptions) {
  const blogDir = './static/content/blog'
  const blogEntryArray = []
  fs.readdirSync(blogDir).forEach(filename => {
    const fcontent = fs.readFileSync(`${blogDir}/${filename}`, 'utf8').replace(/\\n/g,'\\\\n')
    const entry = JSON.parse(fcontent)
    blogEntryArray.push({
      id: filename.substring(0, filename.indexOf('.json')),
      date: entry.date,
      title: entry.title,
      description: entry.description,
      body: entry.body
    })
  });  

  this.addPlugin({
    src: path.resolve(__dirname, 'blog-plugin.js'),
    options: {
      blogEntries: JSON.stringify(blogEntryArray)
    }
  })

}