/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      spacing: {
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
        '108': '27rem',
        '128': '36rem',
        '196': '50rem',
        '256': '72rem',
      },
      borderWidth: {
        '16': '16px'
      }  
    }
  },
  variants: {},
  plugins: []
}
